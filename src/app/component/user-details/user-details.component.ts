import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit  {
  
  userId: any;
  userData: any;
  constructor(private route: ActivatedRoute, private dataService: DataService, private router: Router ) { }
  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.userId = +params['id']; 
      this.dataService.getUsers(1).subscribe(response => {
        const users = response.data; 
        this.userData = users.find((user:any) => user.id === this.userId);
      });
    });
  }
  cancel() {
    this.router.navigate(['/']);
  }
  
}
