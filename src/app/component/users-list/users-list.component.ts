import { Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { CacheService } from 'src/app/services/cache.service';
import { DataService } from 'src/app/services/data.service';
export interface UserData {
  id: string;
  first_name: string;
  last_name: string;
  email: String;
  avatar: string;
}
@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})

export class UsersListComponent {
  displayedColumns: string[] = ['id', 'first_name', 'last_name', 'actions'];
  dataSource: MatTableDataSource<UserData> = new MatTableDataSource<UserData>([]);
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private dataService: DataService, private cacheService: CacheService) {}

  ngOnInit() {
    const userId = 1;
    const cacheKey = `users_${userId}`;

    // Check if data is already in the cache
    const cachedData = this.cacheService.get(cacheKey);
    if (cachedData) {
      this.handleData(cachedData);
    } else {
      // If not in cache, make the HTTP request
      this.dataService.getUsers(userId).subscribe(
        (response) => {
          const data = response.data;
          this.handleData(data);

          // Note: Caching is handled within the service, so no need to cache here explicitly
        },
        (error) => {
          console.error('Error fetching users:', error);
          // Handle error
        }
      );
    }
  }

  private handleData(data: UserData[]): void {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    console.log('Data Source Length:', this.dataSource.data.length);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
